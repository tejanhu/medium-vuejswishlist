import Vue from 'vue'
import Router from 'vue-router'
// import Hello from '@/components/HelloWorld'
import Lists from '@/components/Lists'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/lists',
      name: 'Lists',
      component: Lists
    }
  ]
})
